package service;

import domain.*;
import domain.Friendship;
import domain.Tuple;
import domain.User;
import repository.Repository;
import domain.validators.ValidationException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.function.Predicate;

public class FriendshipService {
    Repository<Long, User> repoUser;
    Repository<Tuple<Long, Long>, Friendship> repoFriendship;

    /**
     * @param repoUser
     * @param repoFriendship
     */
    public FriendshipService(Repository repoUser, Repository<Tuple<Long, Long>, Friendship> repoFriendship) {
        this.repoUser = repoUser;
        this.repoFriendship = repoFriendship;
    }

    /**
     * @param entity
     * @return entity if saved
     * throw Validate Exception if the ID already exists or it is invalid
     */
    public Friendship add(Friendship entity) {
        Long id1 = entity.getE1();
        Long id2 = entity.getE2();

        if (repoUser.findOne(id1) == null || repoUser.findOne(id2) == null)
            throw new ValidationException("Id invalid");
        if (repoFriendship.findOne(entity.getId()) != null)
            throw new ValidationException("Already exists");

        repoUser.findOne(id1).addFriend(repoUser.findOne(id2));
        repoUser.findOne(id2).addFriend(repoUser.findOne(id1));
        LocalDateTime dateTime = LocalDateTime.now();

        entity.setDate(dateTime);
        return repoFriendship.save(entity);
    }


    /**
     * @param id1 must be not null
     * @param id2 must be not null
     */
    public void removeFriendship(Long id1, Long id2) {
        if (repoUser.findOne(id1) == null || repoUser.findOne(id2) == null)
            throw new ValidationException("Nu exista utilizatorii introdusi!");
        Tuple<Long, Long> ship = new Tuple<>(id1, id2);
        repoFriendship.remove(repoFriendship.findOne(ship));
        //repoFriendship.setFriendships();
    }

}