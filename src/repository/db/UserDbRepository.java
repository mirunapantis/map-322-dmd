package repository.db;

import domain.User;
import repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class UserDbRepository implements Repository<Long, User> {
    private String url;
    private String username;
    private String password;


    public UserDbRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public User findOne(Long aLong) {

        String sql = "SELECT * FROM users WHERE id = " + String.valueOf(aLong);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                Long id = resultSet.getLong("id");
                // if(id == aLong) {
                    String firstName = resultSet.getString("first_name");
                    String lastName = resultSet.getString("last_name");
                    User utilizator = new User(firstName, lastName);
                    utilizator.setId(id);
                    return utilizator;
               // }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<User> findAll() {
        Set<User> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                User utilizator = new User(firstName, lastName);
                utilizator.setId(id);
                String sql = "SELECT * FROM friendships f WHERE f.first_friend = " + String.valueOf(utilizator.getId())+ " OR f.second_friend = " + String.valueOf(utilizator.getId());
                PreparedStatement statement1 = connection.prepareStatement(sql);
                ResultSet resultSet1 = statement1.executeQuery();
                while (resultSet1.next()) {
                    Long first = resultSet1.getLong("first_friend");
                    Long second = resultSet1.getLong("second_friend");
                    if(first == utilizator.getId())
                        utilizator.addFriend(findOne(second));
                    if(second == utilizator.getId())
                        utilizator.addFriend(findOne(first));
                    }
                users.add(utilizator);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
    @Override
    public User save(User entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must not be null!");

        String sql = "INSERT INTO users (first_name, last_name ) VALUES (?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User remove(User entity) {

        String sql = "DELETE FROM users WHERE id = ?";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                if (id == entity.getId()) {
                    String firstName = resultSet.getString("first_name");
                    String lastName = resultSet.getString("last_name");

                    User utilizator = new User(firstName, lastName);
                    utilizator.setId(id);

                    PreparedStatement statement1 = connection.prepareStatement(sql);
                    statement1.setInt(1, Integer.parseInt(String.valueOf(id)));
                    statement1.executeUpdate();
                    return utilizator;

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    return null;
    }



    @Override
    public User update(User entity) {
        String sql = "UPDATE FROM users SET first_name = ? WHERE id = ?";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                if (id == entity.getId()) {

                    PreparedStatement statement1 = connection.prepareStatement(sql);
                    statement1.setString(2, entity.getFirstName());
                    statement1.executeUpdate();
                    return entity;

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }





}