package com.company;

import domain.*;
import domain.validators.ValidationException;
import repository.Repository;
import repository.db.FriendshipDbRepository;
import repository.db.UserDbRepository;
import service.FriendshipService;
import service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Repository<Long, User> repoDb = new UserDbRepository("jdbc:postgresql://localhost:5432/socialnetwork", "postgres", "mirup2021");
        Repository<Tuple<Long, Long>, Friendship> repoDbf = new FriendshipDbRepository("jdbc:postgresql://localhost:5432/socialnetwork", "postgres", "mirup2021");

        UserService userService = new UserService(repoDb, repoDbf);
        FriendshipService friendshipService = new FriendshipService(repoDb, repoDbf);

        boolean ok = true;

        while (ok) {
            System.out.println("1.Add an user\n2.Remove an user\n3.Add a friendship\n4.Remove a friendship\n" +
                    "5.Number of conencted components\n6.The most sociable community\n0.Exit");
            System.out.println("Choose an option: ");
            Scanner in = new Scanner(System.in);
            int opt = Integer.parseInt(in.nextLine());
            switch (opt) {
                case 1 -> {

                    System.out.println("First name: ");
                    String fname = in.nextLine();
                    System.out.println("Last name: ");
                    String lname = in.nextLine();

                    User user = new User(fname, lname);

                    try {
                        userService.add(user);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 2 -> {

                    System.out.println("Id: ");
                    long id = Long.parseLong(in.nextLine());
                    try {
                        userService.remove(id);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 3 -> {

                    System.out.println("First id: ");
                    Long fid = Long.parseLong(in.nextLine());
                    System.out.println("Second id: ");
                    Long sid = Long.parseLong(in.nextLine());

                    if (fid > sid) {
                        Long aux = fid;
                        fid = sid;
                        sid = aux;
                    }

                    Tuple<Long, Long> ship = new Tuple<>(fid, sid);
                    Friendship friendship = new Friendship(ship);
                    friendship.setId(ship);
                    try {
                        friendshipService.add(friendship);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 4 -> {

                    System.out.println("First friend id: ");
                    long id = Long.parseLong(in.nextLine());
                    System.out.println("Second friend id: ");
                    long id1 = Long.parseLong(in.nextLine());

                    if (id > id1) {
                        Long aux = id;
                        id = id1;
                        id1 = aux;
                    }

                    try {
                        friendshipService.removeFriendship(id, id1);
                    } catch (ValidationException | IllegalArgumentException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 5 -> System.out.println("The number of connected components is: " + userService.nrConnectedComponents());
                case 6 -> {

                    System.out.println("The most sociable community is: ");
                    userService.sociableCommunity().forEach(System.out::println);
                }
                case 0 -> {

                    System.out.println("Bye!");
                    ok = false;
                }
                default -> System.out.println("Invalid option!");
            }
        }
    }
}

